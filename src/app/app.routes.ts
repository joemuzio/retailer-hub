import {Routes} from '@angular/router';
import {HomeComponent} from './Pages/home/home.component';
import {LoginComponent} from './Pages/login/login.component';
import {AuthGuard} from './Services/AuthGuard';
import {LandingComponent} from './Pages/landing/landing.component';
import {ForecastComponent} from './Pages/forecast/forecast.component';
import {ActualsComponent} from './Pages/actuals/actuals.component';
import {ReportsComponent} from './Pages/reports/reports.component';
import {ReportComponent} from './Pages/report/report.component';

export const routes: Routes = [
  { path: '', redirectTo: "home", pathMatch: "full", canActivate: [AuthGuard] },
  // { path: '/', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login',  component: LoginComponent },
  { path: 'home',   component: HomeComponent, canActivate: [AuthGuard], children: [
      { path: '', redirectTo: 'landing', pathMatch: 'full', canActivate: [AuthGuard] },
      { path: 'landing',   component: LandingComponent, canActivate: [AuthGuard] },
      { path: 'reports',   component: ReportsComponent, canActivate: [AuthGuard] },
      { path: 'forecast',   component: ForecastComponent, canActivate: [AuthGuard] },
      { path: 'actuals',   component: ActualsComponent, canActivate: [AuthGuard] },
      { path: 'report/:messageID', component: ReportComponent, canActivate: [AuthGuard] },
    ]},
  { path: '**',     component: HomeComponent, canActivate: [AuthGuard]}

];
