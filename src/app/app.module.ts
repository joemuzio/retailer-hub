import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { routes } from './app.routes';
import { AuthGuard } from './Services/AuthGuard';
import { NgBusyModule } from 'ng-busy';
import { CalendarModule } from 'angular-calendar';
import { CalendarModule as CalModule } from 'primeng/primeng';
import {DialogModule, ButtonModule, InputTextModule, SelectButtonModule,
  MultiSelectModule, CheckboxModule, TooltipModule, DataTableModule, DropdownModule,
PanelModule, AccordionModule, MenuModule, ConfirmationService} from 'primeng/primeng';
import {DataViewModule} from 'primeng/dataview';

import { AppComponent } from './app.component';
import {WebserviceController} from './Services/WebserviceController';
import { HomeComponent } from './Pages/home/home.component';
import { LoginComponent } from './Pages/login/login.component';
import { ForecastComponent } from './Pages/forecast/forecast.component';
import { ActualsComponent } from './Pages/actuals/actuals.component';
import { LandingComponent } from './Pages/landing/landing.component';
import { NavBarComponent } from './Shared/nav-bar/nav-bar.component';
import { SideMenuComponent } from './Shared/side-menu/side-menu.component';
import { ForecastUsComponent } from './Pages/forecast/us/forecast-us/forecast-us.component';
import { ForecastCalendarComponent } from './Pages/forecast/forecast-calendar/forecast-calendar.component';
import { ForecastMorrisonsComponent } from './Pages/forecast/uk/morrisons/forecast-morrisons/forecast-morrisons.component';
import { ForecastGridComponent } from './Pages/forecast/forecast-grid/forecast-grid.component';
import { ForecastGridUsComponent } from './Pages/forecast/forecast-grid/us/forecast-grid-us/forecast-grid-us.component';
import { ForecastGridMorrisonsComponent } from './Pages/forecast/forecast-grid/uk/forecast-grid-morrisons/forecast-grid-morrisons.component';
import { ForecastGridSainsburyComponent } from './Pages/forecast/forecast-grid/uk/forecast-grid-sainsbury/forecast-grid-sainsbury.component';
import {UserModel} from './Models/UserModel';
import { ForecastSainsburyComponent } from './Pages/forecast/uk/sainsbury/forecast-sainsbury/forecast-sainsbury.component';
import { ProfileMenuComponent } from './Shared/profile-menu/profile-menu.component';
import { AccordionComponent } from './Shared/accordion/accordion.component';
import {CSVExportService} from './Services/CSVExportService';
import {CurrencyPipe, DatePipe} from '@angular/common';
import { ReportsComponent } from './Pages/reports/reports.component';
import { ReportComponent } from './Pages/report/report.component';
import {MSTRController} from './Services/MSTRController';
import {AnalyticsService} from './Services/AnalyticsService';
import { ErrorComponent } from './Shared/error/error.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ForecastComponent,
    ActualsComponent,
    LandingComponent,
    NavBarComponent,
    SideMenuComponent,
    ForecastUsComponent,
    ForecastCalendarComponent,
    ForecastMorrisonsComponent,
    ForecastGridComponent,
    ForecastGridUsComponent,
    ForecastGridMorrisonsComponent,
    ForecastGridSainsburyComponent,
    ForecastSainsburyComponent,
    ProfileMenuComponent,
    AccordionComponent,
    ReportsComponent,
    ReportComponent,
    ErrorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    NgBusyModule,
    DialogModule,
    ButtonModule,
    MultiSelectModule,
    CalModule,
    TooltipModule,
    CalendarModule.forRoot(),
    CheckboxModule,
    SelectButtonModule,
    DataTableModule,
    DataViewModule,
    AccordionModule,
    DropdownModule,
    MenuModule,
    PanelModule,
    InputTextModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  providers: [WebserviceController, AuthGuard, UserModel, CSVExportService,
    CurrencyPipe, DatePipe, MSTRController, AnalyticsService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
