import {Component, EventEmitter, Input, OnInit, Output, SimpleChange} from '@angular/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.css']
})
export class AccordionComponent implements OnInit {
  @Input() data = [];
  @Output() buttonPressed = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }
  accordionButtonPressed(buttonID){
    for(var i = 0; i < this.data.length; i++){
      var btn = (<HTMLInputElement>document.getElementById('button' + i));
      if(btn.value != buttonID){ btn.checked = false;
      }
    }
  }
  tabButtonClicked(buttonValue){
    this.buttonPressed.emit(buttonValue);
  }
}
