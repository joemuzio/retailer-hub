import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfirmationService, ConfirmDialogModule} from "primeng/primeng";

const badCredentialsError = "Invalid credentials. Please try again.";
const networkError = "Network issue please try again. If problem persists please contact support.";
const iosVersionTooLow = "iOS 10 or greater is required to access this site.";

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  @ViewChild('autoShownModal') public autoShownModal:ConfirmDialogModule;
  isModalShown:boolean = false;
  errorMessage = "";

  constructor(private confirmationService: ConfirmationService) {}

  ngOnInit() {

  }
  getErrorMessage(error): string{
    console.log(JSON.stringify(error));
    if(error.error.error == "invalid_grant"){
      return badCredentialsError;
    }
    else{
      return networkError;
    }
  }

  showErrorModalForError(error){
    this.errorMessage = this.getErrorMessage(error);
    this.showModal();
  }
  showNetworkError(){
    this.errorMessage = networkError;
    this.showModal();
  }
  showiOSTooLowModal(){
    this.errorMessage = iosVersionTooLow;
    this.showModal();
  }


  public showModal():void {
    this.isModalShown = true;
  }

}
