import {Component, ElementRef, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {UserModel} from '../../Models/UserModel';
import {DomSanitizer} from '@angular/platform-browser';
import {WebserviceController} from '../../Services/WebserviceController';
import {Subscription} from 'rxjs/Subscription';
import {MSTRController} from '../../Services/MSTRController';

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.css']
})
export class ProfileMenuComponent implements OnInit {
  busy: Subscription;
  @ViewChild('sideMenu') sideMenu: ElementRef;
  data = [];

  public isOpen:boolean = false;


  constructor(private router: Router, public user: UserModel, private sanitizer: DomSanitizer,
              private webservice: WebserviceController, private mstrController: MSTRController) {
    window.onresize = (e) =>
    {
      this.resizeMenu();
    }
  }

  ngOnInit() {

  }
  openMenu(){
    if(this.isOpen == false){
      this.isOpen = true;
      this.resizeMenu();
    }
  }

  closeMenu() {
    if(this.isOpen == true){
      this.isOpen = false;
      if(window.innerWidth >= 768) {
        this.sideMenu.nativeElement.style.right = "calc(-300px)";
      } else {
        this.sideMenu.nativeElement.style.right = "calc(-300px)";
      }
    }
  }
  getRetailerLogo(){
    return this.sanitizer.bypassSecurityTrustUrl('data:image/png;base64,'+this.user.selectedRetailerLogo);
  }
  retailerSelected(retailerID){
    this.closeMenu();
    this.busy = this.webservice.switchRetailer(retailerID).subscribe(
      response => {
        this.busy = this.webservice.updateUserProfile().subscribe(
          response => {
            this.retailerSwitch(true);
          },
          error => {
            this.retailerSwitch(false);
          })
      },
      error => {
        this.retailerSwitch(false);
      })
  }

  retailerSwitch(success:boolean){
    if(success){
      this.router.navigate(['home']);
    }
    else{
      this.mstrController.clearHistoryList();
      this.router.navigate(['home']);
    }
  }
  resizeMenu(){
    if(this.isOpen){
      this.sideMenu.nativeElement.style.right = "calc(0px)";
    } else {
      this.sideMenu.nativeElement.style.right = "calc(-300px)";
    }
  }
  logout(){
    this.closeMenu();
    this.user.logout();
    this.router.navigate(['/login']);
  }
}
