import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {UserModel} from '../../Models/UserModel';
import {MSTRController} from '../../Services/MSTRController';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  busy:Subscription;
  @ViewChild('sideMenu') sideMenu: ElementRef;
  @ViewChild('menuItems') menuItems;

  public isOpen:boolean = false;
  constructor(private router: Router, public user: UserModel, public mstrController: MSTRController) {
    window.onresize = (e) => {
      this.resizeMenu();
    }
  }
  ngOnInit() {

  }
  openMenu(){
    if(this.isOpen == false){
      this.isOpen = true;
      this.resizeMenu();
    }
  }
  closeMenu() {
    if(this.isOpen == true){
      this.isOpen = false;
      if(window.innerWidth >= 768) {
        this.sideMenu.nativeElement.style.left = "-400px";
      } else {
        this.sideMenu.nativeElement.style.left = "-100vw";
      }
    }
  }
  resizeMenu(){
    if(this.isOpen){
      this.sideMenu.nativeElement.style.left = "0px";
    } else {
      this.sideMenu.nativeElement.style.left = "-100%";
    }
  }
  reportSelected(report){
    this.closeMenu();
    this.router.navigate(['home/report/' + report.messageId]);
  }
  goToScreen(screen){
    this.closeMenu();
    var newScreen = 'home/' + screen;
    if(screen == 'home'){ newScreen = 'home'}
    this.router.navigate([newScreen]);
  }
  // logout(){
  //   this.closeMenu();
  //   this.user.removeAll();
  //   this.router.navigate(['login']);
  // }

}
