import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Historylist, HistorylistModel, PlaylistModel} from '../Models/HistoryListModel';

@Injectable()
export class MSTRController{
  public readonly appUrl:string = environment.baseMSTRURL + '/CI/servlet/mstrWeb?';
  historylist = new Historylist();
  constructor(){}

  getURLForID(documentId, userId, token){
    var paramsString = "";
    paramsString += "evt=2048001";
    paramsString += "&src=mstrWeb.2048001";
    paramsString += "&currentViewMedia=1";
    paramsString += "&visMode=0";
    paramsString += "&hiddenSections=header,path,dockTop,dockLeft,footer";
    paramsString += "&Port=0";
    paramsString += "&loginReq=true";
    // paramsString += "&mstrwid=1";
    paramsString += "&Uid=" + userId;
    paramsString += "&cmcID=" + token;
    paramsString += "&messageID=" + documentId;
    if(!environment.production){
      paramsString += "&Project=Retailer%20Insights%20-%20UAT%20-%20US";
      paramsString += "&server=" + environment.mstrServer;
    }
    else{
      paramsString += "&Project=Retailer%20Insights%20-%20US";
      paramsString += "&server=" + environment.mstrServer;
    }
    return this.appUrl + paramsString;
  }
  parseSoapResponse(response):any{
    let startText = "{'data': ";
    let endText = "};";
    var start = response.indexOf(startText);
    var end = response.lastIndexOf(endText);
    if(response.indexOf("exception") > 0 || response.indexOf("'statusCode':    500") > 0) {
      throw "Playlist browse task failed. " + response;
    }
    if(start < 0) {
      start = 0;
    } else {
      start += startText.length;
    }
    if(end < 0) {
      end = (response.length - start);
    }
    let slicedText = response.slice(start, end);
    return JSON.parse(slicedText);
  }
  updatePlaylistReportIds(dataObj:any){
    this.historylist = new Historylist();
    dataObj.data.children.forEach(element => {
      if(element.children && environment.playlistFolderIds.indexOf(element.id) != -1) {
        element.children.forEach(entry => {
          console.log(JSON.stringify(entry));
          let playlistModel = new PlaylistModel();
          playlistModel.id = entry.id;
          playlistModel.name = entry.n;
          playlistModel.image = entry.img;
          this.historylist.data.push(playlistModel);
        });
      }
    });
  }
  updateHistorylistReports(dataObj:any) {
    var sortedArray = dataObj.data.sort(function(a,b){
      return new Date(b.creationTime).getDate() - new Date(a.creationTime).getDate();
    });
    var filteredArray = [];
    for(var i = 0; i < sortedArray.length; i++){
      let report = sortedArray[i];
      var contains = false;
      for(var j = 0; j < filteredArray.length; j++){
        let reportToCheck = filteredArray[j];
        if(reportToCheck.name == report.name){
          contains = true;
        }
      }
      if(contains == false && report.status == "Ready"){
        filteredArray.push(report);
      }
    }
    for(var i = 0; i < filteredArray.length; i++) {
      let report = filteredArray[i];
      for (var j = 0; j < this.historylist.data.length; j++) {
        let dataObj = this.historylist.data[j];
        if (dataObj.id == report.objectId && report.status == "Ready") {
          dataObj.data.push(report);
        }
      }
    }
  }
  clearHistoryList(){
    this.historylist = new Historylist();
  }

}

