import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {tokenNotExpired} from 'angular2-jwt';
import {UserModel} from '../Models/UserModel';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private user:UserModel){

  }
  canActivate(){
    if(tokenNotExpired()){
      return true;
    }
    this.user.logout();
    this.router.navigate(['/login']);
    return false;
  }
}
