import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/forkJoin';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserModel} from '../Models/UserModel';
import {MSTRController} from './MSTRController';

const loginHeaders = new HttpHeaders({'accept': 'application/json', 'content-type': 'application/x-www-form-urlencoded',
  'Authorization': 'Basic ' + btoa('rp_app:rp_app')});

@Injectable()
export class WebserviceController {
  constructor(private http: HttpClient, private user: UserModel, private mstrController: MSTRController) {}
  public login(username, password): Observable<any> {
    let body = 'grant_type=password&username=' + username + '&password=' + encodeURIComponent(password);
    return this.http.post(environment.loginURL(), body, {headers: loginHeaders})
      .timeout(8000)
  }
  public getNARDistributionURLAndToken(){
    let userData = this.getUserData();
    let mstrToken = this.getMSTRToken();
    return Observable.forkJoin([userData, mstrToken]);
  }
  public getUserData(): Observable<any>{
    return this.http.get(environment.userDataURL(), {headers: this.getTokenHeaders()});
  }
  public getNARForecastData(): Observable<any>{
    return this.http.get(environment.narForecastURL(), {headers: this.getTokenHeaders()});
  }
  public getMSTRToken(): Observable<any>{
    return this.http.get(environment.mstrTokenURL(), {headers: this.getTokenHeaders()});
  }
  public switchRetailer(retailerID): Observable<any>{
    return this.http.post(environment.switchRetailerURL(retailerID), null, {headers: this.getTokenHeaders()});
  }
  private getPlaylistData(): Observable<any>{
      return this.http.get(environment.playlistDataURL(), {headers: this.headersTextHTML(), responseType:'text'});
  }
  private getHistorylistData(): Observable<any>{
    return this.http.get(environment.historyListURL(), {headers: this.headersTextHTML(), responseType:'text'});
  }
  public getMSTRListData():Observable<any>{
    let playlistData = this.getPlaylistData();
    let historyListData = this.getHistorylistData();
    return Observable.forkJoin([playlistData, historyListData]);
  }
  private getTokenHeaders(): HttpHeaders{
    var tokenHeaders = new HttpHeaders({'Authorization': 'Bearer ' + this.user.authToken});
    return tokenHeaders
  }
  getValidatingUPCListForPromotion(promotion): Observable<any>{
    var headers = new HttpHeaders({'Authorization': 'Bearer ' + this.user.authToken,
      "Content-Type": "application/octet-stream", "Accept": "application/octet-stream"});
    return this.http.get(environment.validatingUPCURL(promotion), {headers: headers, responseType: "blob" });
  }
  getTriggerUPCListForPromotion(promotion): Observable<any>{
    var headers = new HttpHeaders({'Authorization': 'Bearer ' + this.user.authToken,
      "Content-Type": "application/octet-stream", "Accept": "application/octet-stream"});
    return this.http.get(environment.triggeringUPCURL(promotion), {headers: headers, responseType: "blob" });
  }
  public updateUserProfile(): Observable<any>{
    return new Observable(observer => {
      this.getUserData().subscribe(
        response => {
          this.user.profile = response;
          this.user.updateAvailableRetailers();
          observer.next("complete");
          observer.complete();

        },
        error => {
          observer.error("error");
          observer.complete();
        }
      )
    })
  }

  public updateHistoryList(): Observable<any>{
    return new Observable(observer => {
      this.getMSTRListData().subscribe(
        response => {
          if(response.toString().includes("Could not get session for user") == false){
            this.mstrController.updatePlaylistReportIds(this.mstrController.parseSoapResponse(response[0]));
            this.mstrController.updateHistorylistReports(this.mstrController.parseSoapResponse(response[1]));
          }
          observer.next("complete");
          observer.complete();

        },
        error => {
          observer.error("error" + error.message);
          observer.complete();
        }
      )
    })
  }
  private headersTextHTML(){
    return new HttpHeaders({'Authorization': 'Bearer ' + this.user.authToken,
      'Accept': "text/html", "Content-Type": "text/html"});
  }
}
