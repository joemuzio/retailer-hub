import {Injectable} from '@angular/core';
import {CurrencyPipe, DatePipe} from '@angular/common';
import {UserModel} from '../Models/UserModel';


@Injectable()
export class CSVExportService{
  currencyLocale = "USD";
  fieldsToHide = ["parentCorpKey", "sourceCode", "programNumber", "imageURL"];

  constructor(private currencyPipe:CurrencyPipe, private user: UserModel, private datePipe:DatePipe){
    if(this.user.selectedRetailer == "Morrisons" || this.user.selectedRetailer == "Sainsbury"){
      this.currencyLocale = "GBP"
    }
    this.currencyPipe = new CurrencyPipe('en-US');
    this.datePipe = new DatePipe('en_US');
  }

  createCSVFromData(filteredData, chainNames){
    if(filteredData.length > 0){
      var csvString = "";
      let headerKeys = this.getHeaderKeys(filteredData[0], chainNames);
      let headers = this.getHeadersForKeys(headerKeys);
      csvString += headers;
      for(var i = 0; i < filteredData.length; i++){
        let offer = filteredData[i];
        csvString += this.getCSVLineDataForOffer(offer, headerKeys);
      }
      return csvString;
    }
    return null;
  }
  getHeaderKeys(dataObj, chainNames){
    var headerKeys = [];
    let keys = Object.keys(dataObj);
    for(let key in keys){
      if(keys[key] != "chains"){
        var shouldShow = true;
        for(var i = 0; i < this.fieldsToHide.length; i++){
          if(this.fieldsToHide[i] == keys[key]){
            shouldShow = false;
          }
        }
        if(shouldShow){
          headerKeys.push(keys[key]);
        }

      }
    }
    if(chainNames){
      for(var i = 0; i < chainNames.length; i++){
        let str = chainNames[i].label;

        headerKeys.push(str);
      }
    }
    return headerKeys;
  }
  getHeadersForKeys(headerKeys){
    var headers = "";
    for(var i = 0; i < headerKeys.length; i++){
      headers += headerKeys[i] + ",";
      if(headerKeys.length - 1 == i){
        headers += "\n"
      }
    }
    return headers;
  }
  getCSVLineDataForOffer(offer, headerKeys){
    var csvString = "";
    let offerKeys = Object.keys(offer);
    for(let key in offerKeys){
      if(offerKeys[key] != "chains"){
        if(offerKeys[key] == "value"){
          let value = parseFloat(offer[offerKeys[key]]);
          csvString += this.currencyPipe.transform(value, this.currencyLocale, "symbol", '1.2-2') + ",";
        }
        else if(offerKeys[key] == "start" || offerKeys[key] == 'endDate'){
          csvString += this.datePipe.transform(offer[offerKeys[key]], 'shortDate') + ","
        }
        else{
          var shouldShow = true;
          for(var i = 0; i < this.fieldsToHide.length; i++){
            if(this.fieldsToHide[i] == offerKeys[key]){
              shouldShow = false;
            }
          }
          if(shouldShow){
            csvString += '"' + offer[offerKeys[key]] + '",';
          }
        }
      }

      else{
        if(offer.chains){
          var containsChain = false;
          for(var k = 0; k < offer.chains.length; k++){
            let chainName = offer.chains[k].chainName;
            for(var i = 0; i < headerKeys.length; i++){
              let headerName = headerKeys[i];
              if(chainName == headerName){
                containsChain = true;
              }
            }
            if(containsChain){
              csvString += "X,";
            }
            else{
              csvString += ",";
            }
          }
        }
      }
    }
    csvString += "\n";
    return csvString;
  }

}
