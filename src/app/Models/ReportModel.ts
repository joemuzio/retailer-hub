
export class ReportModel{
  reportCategory:String = "";
  reportTitle:String = "";
  reportDate: Date = new Date();
  reportURL:String = "";
  reportIconURL:String = "";
  reportIsMSTR:boolean = false;
  reportID:String = "";
  reportImage:string = "";
  items:Array<any> = [];
  constructor(){
  }
}
