import {ChainModel} from '../ChainModel';

export class OfferModel{
  manufacturer:string = "''";
  category:string = "";
  start:Date = new Date('1/1/1980');
  endDate:Date = new Date('1/1/1980');
  color:{};
  source:string = "";
  promotionNumber:string = "";
  value:number = 0.0;
  type:string = "";
  couponType:string = "";
  parentCorpKey:string = "";
  objective:string = "";
  brand:string = "''";
  isFree:boolean = false;
  isAdvertised:boolean = false;
  isAccountSpecific:boolean = false;
  isCCM:boolean = false;
  isActive:boolean = false;
  chains:Array<ChainModel> = [];
  showValidatingUPCs = false;
  showTriggeringUPCs = false;

  constructor(data){
    if(data['mfg_nm']){ this.manufacturer = data['mfg_nm']; }
    if(this.manufacturer == "" || this.manufacturer == " "){
      if(data['client_nm']){ this.manufacturer = data['client_nm']; }
    }
    if(data['brand']){ this.brand = data['brand']; }
    if(data['cmc_cat_desc']){ this.category = data['cmc_cat_desc']; }
    if(data['start_dt']){ this.start = new Date(data['start_dt']); }
    if(data['stop_dt']){ this.endDate = new Date(data['stop_dt']); }
    if(data['promo_typ_cd']){ this.source = data['promo_typ_cd']; }
    if(data['mclu_nbr']){ this.promotionNumber = data['mclu_nbr']; }
    if(data['coup_val_amt']){ this.value = parseFloat(data['coup_val_amt']); }
    if(data['tgt_typ_descr']){ this.type = data['tgt_typ_descr']; }
    if(data['promo_cpblty_typ_cd']){ this.couponType = data['promo_cpblty_typ_cd']; }
    if(data['parent_corp_key']){ this.parentCorpKey = data['parent_corp_key']; }
    if(data['sls_objective']){ this.objective = data['sls_objective']; }
    if(data['free_flg'] && (data['free_flg'] == 'Y' || data['free_flg'] == 'y')){
      this.isFree = true
    }
    if(data['advertised_flg'] && (data['advertised_flg'] == 'Y' || data['advertised_flg'] == 'y')){
      this.isAdvertised = true
    }
    if(data['acct_spec_flg'] && (data['acct_spec_flg'] == 'Y' || data['acct_spec_flg'] == 'y')){
      this.isAccountSpecific = true;
    }
    if(this.couponType == "CCM" || this.couponType == "ccm"){
      this.isCCM = true;
    }
    if(data['chains']){
      var isActive = true;
      for (var i = 0; i < data['chains'].length; i++) {
        var chainData = data['chains'][i];
        var chain = new ChainModel(chainData['cmc_chn_nbr'], chainData['cmc_chn_nm'], chainData['clu_on_flg']);
        this.chains.push(chain);
        if(chain.onFlag == false){ isActive = false; }
      }
      this.isActive = isActive;
    }

    if(this.type == "Historical Behavior" && this.source != "Retailer"){
      this.showValidatingUPCs = true;
    }
    if(this.type == "Current Behavior"){
      this.showTriggeringUPCs = true;
      if(this.isCCM == false && this.source != "Retailer"){
        this.showValidatingUPCs = true;
      }
    }

  }
}

