export class Historylist{
  data:Array<PlaylistModel> = [];
}
export class PlaylistModel{
      name: string = "";
      id: string = "";
      image: string = "";
      data:Array<HistorylistModel> = [];
}
export class HistorylistModel{
  objectId:string = "";
  messageId:string = "";
  creationTime: Date;
  name:string = "";
  status:string = "";
  type:string = "";
}
