import {Injectable} from '@angular/core';

@Injectable()
export class UserModel{
  private _profile:ProfileModel;
  get profile():ProfileModel{
    if(!this._profile){
      this._profile = new ProfileModel();
    }
    return this._profile;
  }
  set profile(profile){
    this._profile = profile;
  }
  get selectedRetailer():string{
    return this.profile.retailer.retailerName;
  }
  get selectedRetailerLogo():string{
    return this.profile.retailer.retailerImage;
  }
  get authToken():string{
    return localStorage.getItem("token");
  }
  set authToken(token:string){
    localStorage.setItem("token", token);
  }

  // get availableRetailer():Array<any>{
  //   return this.profile.retailers;
  // }
  // keys = {
  //   authTokenKey:"token",
  // };

  constructor(){}
  logout(){
    localStorage.removeItem("token");
  }

  updateAvailableRetailers(){
    var retailersArray = [];
    for(var i = 0; i < this._profile.retailers.length; i++){
      let retailer = this._profile.retailers[i]['retailer'];
      let obj = {
        label: retailer.retailerName,
        value: retailer.id
      };
      if(this._profile.retailer.id != retailer.id){
        retailersArray.push(obj);
      }
    }
    retailersArray.sort(this.compare);
    if(retailersArray.length > 0){
      let formattedArray = [
        {
          label: "Retailer Select",
          data: retailersArray
        }
      ];
      this._profile.availableRetailers = formattedArray;
    }
  }
  compare(a,b) {
    if (a.label < b.label)
      return -1;
    if (a.label > b.label)
      return 1;
    return 0;
  }
}
export class ProfileModel {

  id: null;

  firstName: string = "";
  lastName: string = "";
  userName: string = "";
  emailAddress: string = "";
  deletedInd: "N";
  userConfig: {
    id: null,
    configData: "Test Config Data"
  };
  telephone: string = "";
  companyName: string = "";
  jobTitle: string = "";
  isExternal: string = "Y";
  createdBy: null;
  createdDate: null;
  modifiedBy: null;
  modifiedDate: null;
  requestorEmail: null;
  retailer: RetailerModel = new RetailerModel();
  retailers: Array<RetailerModel> = [];
  availableRetailers: Array<any> = [];
  userRoled:Array<any> = [];

}

export class RetailerModel{
  id:string = "";
  retailerName:string = "";
  retailerURL:string = "";
  retailerImage:string = "";
  retailerImageURL:string = "";
  documentId:string = "";
  dataPath: "";
  narDistFileName: "";
  narForecastFileName: "";
  mstrNarDistDocumentId: "";
  validateReports: true;

}

