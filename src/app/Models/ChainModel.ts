export class ChainModel{
  chainNumber:number = 0;
  chainName:string = "";
  onFlag:boolean = false;
  constructor(chainNumber:number, chainName:string, onFlag:string){
    this.chainNumber = chainNumber;
    this.chainName = chainName;
    if(onFlag == "Y" || onFlag == 'y'){
      this.onFlag = true;
    }
  }
}
