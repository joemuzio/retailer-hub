import {ChainModel} from '../../ChainModel';

export class OfferModel{
  start:Date = new Date('1/1/1980');
  endDate:Date = new Date('1/1/1980');
  color:{};
  sourceCode:number = 0;
  source:string = "";
  promotionNumber:string = "";
  value:number = 0.0;
  type:string = "";
  couponType:string = "";
  parentCorpKey:string = "";
  businessArea:string = "";
  brand:string = "";
  campaignDescription:string = "";
  programNumber:number = 0;
  chains:Array<ChainModel> = [];

  constructor(data){
    if(data['start_dt']){ this.start = new Date(data['start_dt']); }
    if(data['stop_dt']){ this.endDate = new Date(data['stop_dt']); }
    if(data['d_order_nbr']){ this.sourceCode = data['d_order_nbr']; }
    if(data['mclu_nbr']){ this.promotionNumber = data['mclu_nbr']; }
    if(data['coup_val_amt']){ this.value = parseFloat(data['coup_val_amt']); }
    if(data['custcol_85']){ this.type = data['custcol_85']; }
    if(data['custcol_81']){ this.couponType = data['custcol_81']; }
    if(data['parent_corp_key']){ this.parentCorpKey = data['parent_corp_key']; }
    if(data['f_business_area']){ this.businessArea = data['f_business_area']; }
    if(data['d_brand']){ this.brand = data['d_brand']; }
    if(data['pgm_descr']){ this.campaignDescription = this.replaceUnderScores(data['pgm_descr']); }
    if(data['cmc_prog_nbr']){ this.programNumber = data['cmc_prog_nbr'] }
    this.source = this.getSourceForSourceCode();

    if(data['chains']){
      var isActive = true;
      for (var i = 0; i < data['chains'].length; i++) {
        var chainData = data['chains'][i];
        var chain = new ChainModel(chainData['cmc_chn_nbr'], chainData['cmc_chn_nm'], chainData['clu_on_flg']);
        this.chains.push(chain);
        if(chain.onFlag == false){ isActive = false; }
      }
    }
  }

  private getSourceForSourceCode(){
    if(this.sourceCode == 19265){
      return "Local";
    }
    else if(this.sourceCode == 16185 || this.sourceCode == 13266 || this.sourceCode == 13267){
      return "Retail";
    }
    else{
      return "Supplier";
    }
  }

  replaceUnderScores(str:string):string{
    var updatedString = str.replace(/_/g, ' ');
    return updatedString;
  }

}

