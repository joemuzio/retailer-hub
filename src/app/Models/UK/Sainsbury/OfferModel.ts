export class OfferModel{
  start:Date = new Date('1/1/1980');
  endDate:Date = new Date('1/1/1980');
  color:{};
  sourceCode:number = 0;
  source:string = "";
  promotionNumber:string = "";
  value:number = 0.0;
  type:string = "";
  couponType:string = "";
  parentCorpKey:string = "";
  offerType:string = "";
  brand:string = "";
  campaignDescription:string = "";
  programNumber:number = 0;

  constructor(data){
    if(data['start_dt']){ this.start = new Date(data['start_dt']); }
    if(data['stop_dt']){ this.endDate = new Date(data['stop_dt']); }
    if(data['d_order_nbr']){ this.sourceCode = data['d_order_nbr']; }
    if(data['mclu_nbr']){ this.promotionNumber = data['mclu_nbr']; }
    if(data['coup_val_amt']){ this.value = parseFloat(data['coup_val_amt']); }
    if(data['custcol_85']){ this.type = data['custcol_85']; }
    if(data['custcol_81']){ this.couponType = data['custcol_81']; }
    if(data['parent_corp_key']){ this.parentCorpKey = data['parent_corp_key']; }
    if(data['coup_type']){ this.offerType = data['coup_type']; }
    if(data['d_brand']){ this.brand = data['d_brand']; }
    if(data['pgm_descr']){ this.campaignDescription = this.replaceUnderScores(data['pgm_descr']); }
    if(data['cmc_prog_nbr']){ this.programNumber = data['cmc_prog_nbr'] }
    this.source = this.getSourceForSourceCode();

  }

  private getSourceForSourceCode(){
    switch (this.sourceCode){
      case 10102:
        return "Bank";
      case 10182: case 10183: case 11105: case 14106: case 19457: case 19458: case 19460: case 19458: case 19459:
      case 19447: case 19448: case 19449: case 19450: case 19451: case 19452: case 19453: case 19454: case 19455:
      case 19456:
        return "Retail";
      case 19928: case 19929: case 19930: case 10185: case 9162:
      return "Supplier";
      default:
        return "Other";
    }
  }

  replaceUnderScores(str:string):string{
    var updatedString = str.replace(/_/g, ' ');
    return updatedString;
  }
}

