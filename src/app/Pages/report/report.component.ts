import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MSTRController} from '../../Services/MSTRController';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {WebserviceController} from '../../Services/WebserviceController';
import {UserModel} from '../../Models/UserModel';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  url: SafeResourceUrl;

  constructor(private route: ActivatedRoute, private user: UserModel, private router: Router, private mstrController: MSTRController,
              private webservice: WebserviceController, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.route.params.subscribe(response => {
      console.log(JSON.stringify(response));
    })
    this.webservice.getMSTRToken().subscribe(
      response => {
        console.log(JSON.stringify(response));
        var userId = response.data.userId;
        var token = response.data.token;
        this.route.params.subscribe(params => {
          if(params['messageID'] != null){
            let documentId = params['messageID'];
            console.log(this.mstrController.getURLForID(documentId, userId, token));
             this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.mstrController.getURLForID(documentId, userId, token));
          } else {
            //ToDo 404 report
            console.log("No document id provided.");
            this.router.navigate(['home']);
          }
        });
      },
      error => {
        console.log(JSON.stringify(error));
      }
    );

  }

}
