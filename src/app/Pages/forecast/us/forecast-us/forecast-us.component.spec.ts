import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastUsComponent } from './forecast-us.component';

describe('ForecastUsComponent', () => {
  let component: ForecastUsComponent;
  let fixture: ComponentFixture<ForecastUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
