import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {WebserviceController} from '../../../../../Services/WebserviceController';
import {OfferModel} from '../../../../../Models/UK/Morrisons/OfferModel';
import {CSVExportService} from '../../../../../Services/CSVExportService';
import * as FileSaver from "file-saver";


@Component({
  selector: 'app-forecast-morrisons',
  templateUrl: './forecast-morrisons.component.html',
  styleUrls: ['./forecast-morrisons.component.css']
})
export class ForecastMorrisonsComponent implements OnInit {
  filterNames = {
    businessArea: "businessArea",
    brand: "brand",
    source: "source",
    chain: "chainNumber",
    startDate: "start",
    free: "isFree",
    advertised: "isAdvertised",
    ccm: "isCCM",
    accountSpecific: "isAccountSpecific",
    active: "isActive"
  };

  activeOptions = [
    {label: "All", value: "All"},
    {label: "Active", value: "Active"},
    {label: "Inactive", value: "Inactive"}
  ];

  selectedViews = [];
  currentViewCalendar = true;
  selectedRetailer = "";
  busy: Subscription;
  earliestStartDate: Date = new Date();
  allOffers: Array<OfferModel> = [];
  filteredOffers: Array<OfferModel> = [];

  availableSources = [];
  availableChains = [];
  availableBusinessArea = [];
  availableBrands = [];

  selectedSources = [];
  selectedChains = [];
  selectedBusinessArea = [];
  selectedBrands = [];
  selectedDates = undefined;
  selectedDate = undefined;
  selectedActive = "Active";

  filterFree = false;
  filterAdvertised = false;
  filterAccountSpecific = false;
  filterCCM = false;

  yearRange:string = "";
  constructor(private webservice: WebserviceController, private csvExportService: CSVExportService) {
    this.selectedViews.push({label:'Calendar', value:this.currentViewCalendar});
    this.selectedViews.push({label:'Grid', value:!this.currentViewCalendar});

  }

  ngOnInit() {
    this.busy = this.webservice.getNARForecastData().subscribe(response => {
        if (response != null && response[0]['mfg_nm'] != null) {
          for (var i = 0; i < response.length; i++) {
            let offerData = response[i];
            let offer = new OfferModel(offerData);
            if (offer.start < this.earliestStartDate) {
              this.earliestStartDate = offer.start;
            }
            this.allOffers.push(offer);

          }
          this.updateFilteredData();
        }
        else {
          console.log("network error")
          // this.errorComponent.showNetworkError();
        }

      },
      error => {
        console.log(JSON.stringify(error));
        // this.errorComponent.showErrorModalForError(error);
      });
  }
  updateFilteredData() {
    var dataArray = this.allOffers;
    this.availableChains = this.getChainsForOffers(dataArray);
    this.availableSources = this.getSourcesForOffers(dataArray);
    this.availableBusinessArea = this.getBusinessAreaForOffers(dataArray);
    this.availableBrands = this.getBrandsForOffers(dataArray);
    dataArray = this.filterForName(dataArray, this.selectedBusinessArea, this.filterNames.businessArea);
    dataArray = this.filterForName(dataArray, this.selectedBrands, this.filterNames.brand);
    dataArray = this.filterForName(dataArray, this.selectedSources, this.filterNames.source);
    dataArray = this.filterChainNames(dataArray);
    dataArray = this.filterForDates(dataArray, this.filterNames.startDate, this.selectedDates);
    dataArray = this.filterActiveDate(dataArray, this.selectedDate);
    dataArray = this.filterForBool(dataArray, this.filterFree, this.filterNames.free);
    dataArray = this.filterForBool(dataArray, this.filterCCM, this.filterNames.ccm);
    dataArray = this.filterForBool(dataArray, this.filterAdvertised, this.filterNames.advertised);
    dataArray = this.filterForBool(dataArray, this.filterAccountSpecific, this.filterNames.accountSpecific);
    dataArray = this.filterActiveChains(dataArray);
    this.filteredOffers = dataArray;
  }
  getBusinessAreaForOffers(dataArray) {
    var availableBusinessAreas = [];
    dataArray = this.filterForName(dataArray, this.selectedBrands, this.filterNames.brand);
    dataArray = this.filterForName(dataArray, this.selectedSources, this.filterNames.source);
    dataArray = this.filterChainNames(dataArray);
    dataArray = this.filterForDates(dataArray, this.filterNames.startDate, this.selectedDates);
    dataArray = this.filterActiveDate(dataArray, this.selectedDate);
    dataArray = this.filterForBool(dataArray, this.filterFree, this.filterNames.free);
    dataArray = this.filterForBool(dataArray, this.filterAdvertised, this.filterNames.advertised);
    dataArray = this.filterForBool(dataArray, this.filterCCM, this.filterNames.ccm);
    dataArray = this.filterForBool(dataArray, this.filterAccountSpecific, this.filterNames.accountSpecific);
    dataArray = this.filterActiveChains(dataArray);
    // dataArray = this.filterForBool(dataArray, this.filterActive, this.filterNames.active);

    for (var i = 0; i < dataArray.length; i++) {
      let offer = dataArray[i];
      var contains = false;
      for (var j = 0; j < availableBusinessAreas.length; j++) {
        let offerToCheck = availableBusinessAreas[j];
        if (offer.businessArea == offerToCheck.value) {
          contains = true;
          break;
        }
      }
      if (contains == false) {
        let manufacturerObj = {
          label: offer.businessArea,
          value: offer.businessArea
        };
        availableBusinessAreas.push(manufacturerObj);
      }
    }
    return this.sortArray(availableBusinessAreas);
  }

  getBrandsForOffers(dataArray) {
    var availableBrands = [];
    dataArray = this.filterForName(dataArray, this.selectedBusinessArea, this.filterNames.businessArea);
    dataArray = this.filterForName(dataArray, this.selectedSources, this.filterNames.source);
    dataArray = this.filterChainNames(dataArray);
    dataArray = this.filterForDates(dataArray, this.filterNames.startDate, this.selectedDates);
    dataArray = this.filterActiveDate(dataArray, this.selectedDate);
    dataArray = this.filterForBool(dataArray, this.filterFree, this.filterNames.free);
    dataArray = this.filterForBool(dataArray, this.filterCCM, this.filterNames.ccm);
    dataArray = this.filterForBool(dataArray, this.filterAdvertised, this.filterNames.advertised);
    dataArray = this.filterForBool(dataArray, this.filterAccountSpecific, this.filterNames.accountSpecific);
    dataArray = this.filterActiveChains(dataArray);
    // dataArray = this.filterForBool(dataArray, this.filterActive, this.filterNames.active);

    for (var i = 0; i < dataArray.length; i++) {
      let offer = dataArray[i];
      var contains = false;
      for (var j = 0; j < availableBrands.length; j++) {
        let offerToCheck = availableBrands[j];
        if (offer.brand == offerToCheck.value) {
          contains = true;
          break;
        }
      }
      if (contains == false) {
        let brandObj = {
          label: offer.brand,
          value: offer.brand
        };
        availableBrands.push(brandObj);
      }
    }
    return this.sortArray(availableBrands);

  }

  getSourcesForOffers(dataArray) {
    var availableSources = [];
    dataArray = this.filterForName(dataArray, this.selectedBusinessArea, this.filterNames.businessArea);
    dataArray = this.filterForName(dataArray, this.selectedBrands, this.filterNames.brand);
    dataArray = this.filterChainNames(dataArray);
    dataArray = this.filterForDates(dataArray, this.filterNames.startDate, this.selectedDates);
    dataArray = this.filterActiveDate(dataArray, this.selectedDate);
    dataArray = this.filterForBool(dataArray, this.filterFree, this.filterNames.free);
    dataArray = this.filterForBool(dataArray, this.filterCCM, this.filterNames.ccm);
    dataArray = this.filterForBool(dataArray, this.filterAdvertised, this.filterNames.advertised);
    dataArray = this.filterForBool(dataArray, this.filterAccountSpecific, this.filterNames.accountSpecific);
    dataArray = this.filterActiveChains(dataArray);
    // dataArray = this.filterForBool(dataArray, this.filterActive, this.filterNames.active);

    for (var i = 0; i < dataArray.length; i++) {
      let offer = dataArray[i];
      var contains = false;
      for (var j = 0; j < availableSources.length; j++) {
        let offerToCheck = availableSources[j];
        if (offer.source == offerToCheck.value) {
          contains = true;
          break;
        }
      }
      if (contains == false) {
        let sourceObj = {
          label: offer.source,
          value: offer.source
        };
        availableSources.push(sourceObj);
      }
    }
    return this.sortArray(availableSources);

  }

  getChainsForOffers(dataArray) {
    var availableChains = [];
    var tempArray = [];
    dataArray = this.filterForName(dataArray, this.selectedBusinessArea, this.filterNames.businessArea);
    dataArray = this.filterForName(dataArray, this.selectedBrands, this.filterNames.brand);
    dataArray = this.filterForName(dataArray, this.selectedSources, this.filterNames.source);
    dataArray = this.filterForDates(dataArray, this.filterNames.startDate, this.selectedDates);
    dataArray = this.filterActiveDate(dataArray, this.selectedDate);
    dataArray = this.filterForBool(dataArray, this.filterFree, this.filterNames.free);
    dataArray = this.filterForBool(dataArray, this.filterAdvertised, this.filterNames.advertised);
    dataArray = this.filterForBool(dataArray, this.filterAccountSpecific, this.filterNames.accountSpecific);
    dataArray = this.filterActiveChains(dataArray);
    // dataArray = this.filterForBool(dataArray, this.filterActive, this.filterNames.active);
    dataArray = this.filterForBool(dataArray, this.filterCCM, this.filterNames.ccm);

    for (var i = 0; i < dataArray.length; i++) {
      let offer = dataArray[i];
      let chains = offer.chains;
      for (var j = 0; j < chains.length; j++) {
        let chain = chains[j];
        tempArray.push(chain);
      }
    }
    for (var i = 0; i < tempArray.length; i++) {
      var contains = false;
      let chain = tempArray[i];
      for (var j = 0; j < availableChains.length; j++) {
        let chainToCheck = availableChains[j];
        if (chainToCheck.value == chain.chainNumber) {
          contains = true;
          break;
        }
      }
      if (contains == false) {
        // if ((this.filterActive == true && chain.onFlag == true) || this.filterActive == false) {
        let chainObj = {
          label: chain.chainName,
          value: chain.chainNumber
        };
        availableChains.push(chainObj)
        // }
      }
    }
    return this.sortArray(availableChains);
  }

  filterForName(dataArray, selectedArray, filter): Array<any> {
    if (selectedArray.length > 0) {
      var filteredData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let offer = dataArray[i];
        for (var j = 0; j < selectedArray.length; j++) {
          let filterObj = selectedArray[j];
          if (filterObj == offer[filter]) {
            filteredData.push(offer);
          }
        }
      }
      return filteredData;
    }
    else {
      return dataArray
    }
  }

  filterForBool(dataArray, filterBool, filter) {
    if (filterBool == true) {
      var filteredData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let offer = dataArray[i];
        if (offer[filter] == true) {
          filteredData.push(offer);
        }
      }
      return filteredData;
    }
    else {
      return dataArray;
    }
  }

  filterForDates(dataArray, startFilter, dateRanges) {
    if (dateRanges != null && dateRanges.length > 0) {
      var filteredData = [];
      for (var i = 0; i < dataArray.length; i++) {
        var isWithin = false;
        let offer = dataArray[i];
        let offerStart = offer[startFilter];
        if (dateRanges.length == 2 && dateRanges[1] != null) {
          let startDate = dateRanges[0];
          let endDate = dateRanges[1];
          isWithin = this.datesWithin(offerStart, startDate, endDate);
        }
        else {
          let startDate = dateRanges[0];
          if (this.datesEqual(startDate, offerStart)) {
            isWithin = true;
          }
        }
        if (isWithin) {
          filteredData.push(offer);
        }
      }
      return filteredData
    }
    else {
      return dataArray;
    }
  }
  filterActiveDate(dataArray, activeDate){
    if(activeDate != null){
      var filteredData = [];
      for(var i = 0; i < dataArray.length; i++){
        let offer = dataArray[i];
        let offerStart = offer.start;
        let offerEnd = offer.endDate;
        if(this.datesWithin(activeDate, offerStart, offerEnd) == true){
            filteredData.push(offer);
        }
      }
      return filteredData;
    }
    return dataArray;
  }

  filterChainNames(dataArray) {
    if (this.selectedChains.length > 0) {
      var filteredArray = [];
      for (var i = 0; i < dataArray.length; i++) {
        let offer = dataArray[i];
        if (this.chainContainsChainNumber(offer.chains)) {
          filteredArray.push(offer);
        }
      }
      return filteredArray;
    }
    return dataArray;
  }

  chainContainsChainNumber(chains) {
    var contains = false;
    for (var i = 0; i < chains.length; i++) {
      let chain = chains[i];
      for (var j = 0; j < this.selectedChains.length; j++) {
        let chainToCheck = this.selectedChains[j];
        if (chainToCheck == chain.chainNumber) {
          contains = true;
          break;
        }
      }
    }
    return contains;
  }
  filterActiveChains(dataArray){
    var filteredArray = [];
    for(var i = 0; i < dataArray.length; i++){
      var isActive = false;
      let offer = dataArray[i];
      if(offer.programNumber == 99){
        isActive = true;
      }
      if(this.selectedActive == "All" || (this.selectedActive == "Active" && isActive == true) ||
        (this.selectedActive == "Inactive" && isActive == false)){
        filteredArray.push(offer);
      }
    }
    return filteredArray;

  }
  datesWithin(offerStartDate, startDate, endDate){
    var isWithin = false;
    var offerSDate = new Date(offerStartDate);
    var startD = new Date(startDate);
    var endD = new Date(endDate);
    offerSDate.setHours(0,0,0,0);
    startD.setHours(0,0,0,0,);
    endD.setHours(0,0,0,0);
    if(offerSDate.getTime() >= startD.getTime() && offerSDate.getTime() <= endD.getTime()){
      isWithin = true;
    }
    return isWithin;
  }

  datesEqual(date1, date2): boolean {
    let date1year = date1.getFullYear();
    let date2year = date2.getFullYear();
    let date1Month = date1.getMonth();
    let date2Month = date2.getMonth();
    let date1Day = date1.getDate();
    let date2Day = date2.getDate();

    if (date1year == date2year && date1Month == date2Month && date1Day == date2Day) {
      return true;
    }
    else {
      return false;
    }
  }
  sortArray(array){
    array.sort(function (a, b) {
      let textA = a.label.toUpperCase();
      let textB = b.label.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    return array;
  }

  formatDate(date: Date) {
    let newDate = (date.setHours(0,0,0,0));
    return newDate;
  }
  public getYearRange():string{
    let thisYear = new Date().getFullYear();
    let startRange = thisYear - 1;
    let endRange = thisYear + 1;
    let rangeString = startRange + ":" + endRange;
    return rangeString
  }
  clearActiveDate(){
    this.selectedDate = undefined;
    this.updateFilteredData();
  }

  exportButtonPressed(){
    let csv = this.csvExportService.createCSVFromData(this.filteredOffers, this.availableChains);
    if(csv){
      let file = new Blob([csv], {type: 'text/csv;charset=utf-8'});
      FileSaver.saveAs(file, 'PromotionalActivity.csv');
    }
  }

  updateFromChild(event){
    if(event.selectedSource){
      this.selectedDates = [event.selectedDate];
      let source = event.selectedSource.value;
      this.selectedSources = [source];
    }
    else{
      this.selectedDate = event.selectedDate;
    }
    this.updateFilteredData();
    this.currentViewCalendar = false;

  }

}
