import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastMorrisonsComponent } from './forecast-morrisons.component';

describe('ForecastMorrisonsComponent', () => {
  let component: ForecastMorrisonsComponent;
  let fixture: ComponentFixture<ForecastMorrisonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastMorrisonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastMorrisonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
