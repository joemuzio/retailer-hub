import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastSainsburyComponent } from './forecast-sainsbury.component';

describe('ForecastSainsburyComponent', () => {
  let component: ForecastSainsburyComponent;
  let fixture: ComponentFixture<ForecastSainsburyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastSainsburyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastSainsburyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
