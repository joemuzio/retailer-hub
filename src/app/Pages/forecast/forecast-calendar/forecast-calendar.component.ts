import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';
import {subMonths, addMonths, addDays, addWeeks, subDays, subWeeks} from 'date-fns';
import {UserModel} from '../../../Models/UserModel';

interface MyEvent extends CalendarEvent {
  manufacturer: string;
  source:string;
  start:Date;
  color: {primary: string, secondary: string}
}
type CalendarPeriod = 'day' | 'week' | 'month';

function addPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: addDays,
    week: addWeeks,
    month: addMonths
  }[period](date, amount);
}

function subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: subDays,
    week: subWeeks,
    month: subMonths
  }[period](date, amount);
}

@Component({
  selector: 'app-forecast-calendar',
  templateUrl: './forecast-calendar.component.html',
  styleUrls: ['./forecast-calendar.component.css']
})
export class ForecastCalendarComponent implements OnInit {

  @Input() data:Array<any>;
  selectedRetailer:String = "";

  @Output() viewChange = new EventEmitter();

  view: CalendarPeriod = 'month';
  viewDate: Date = new Date();
  groupClicked = false;


  constructor(private user: UserModel) {
    this.selectedRetailer = user.selectedRetailer;
  }

  ngOnInit() {

  }
  increment(): void {
    this.changeDate(addPeriod(this.view, this.viewDate, 1));
  }

  decrement(): void {
    this.changeDate(subPeriod(this.view, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  changeDate(date: Date): void {
    this.viewDate = date;
  }

  eventClicked(event){
    let offer = event[1][0];
    this.groupClicked = true;
    let dataObj = {
      selectedDate: offer.start,
      selectedSource: {
        label: offer.source,
        value: offer.source
      }
    };
    this.viewChange.emit(dataObj);
  }
  dayClicked(event){
    if(this.groupClicked == false) {
      let dataObj = {
        selectedDate: event.date
      };
      this.viewChange.emit(dataObj);
    }
    else{
      this.groupClicked = false;
    }
  }
  beforeMonthViewRender({body}: {body: CalendarMonthViewDay[]}): void {
    body.forEach((cell) => {
      const groups: any = {};
      cell.events.forEach((event: MyEvent) => {
        groups[event.source] = groups[event.source] || [];
        groups[event.source].push(event);
      });

      if (!Object.entries)
        Object.entries = function( obj ){
          var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array
          while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];

          return resArray;
        };

      cell['eventGroups'] = Object.entries(groups);

      let calendarDate = cell.date;
      var count = 0;
      for (var i = 0; i < this.data.length; i++) {
        let event = this.data[i];
        if (calendarDate >= event.start && calendarDate <= event.endDate) {
          count++;
        }
      }
      cell.badgeTotal = count;
    });
  }
  getColorForSource(group){
    let source = group[1][0].source;
    let gray = '#a3abae';
    let darkBlue = '#5E82A3';
    let red = '#E87511';
    switch (this.selectedRetailer){
      case "Morrisons":
        if(source == "Local"){
          return red;
        }
        else if(source == "Retail"){
          return darkBlue;
        }
        else{
          return gray;
        }
      case "Sainsbury":
        if(source == "Supplier"){
          return red;
        }
        else if(source == "Retail"){
          return darkBlue;
        }
        else{
          return gray;
        }
      default:
        if (source == 'Retailer'){
          return darkBlue;
        }
        else if (source == 'Manufacturer'){
          return red;
        }
        else{
          return gray;
        }
    }


  }

}
