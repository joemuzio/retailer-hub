import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {environment} from '../../../../environments/environment';
import {UserModel} from '../../../Models/UserModel';

@Component({
  selector: 'app-forecast-grid',
  templateUrl: './forecast-grid.component.html',
  styleUrls: ['./forecast-grid.component.css']
})
export class ForecastGridComponent implements OnInit {
  @Input() dataArray = [];
  constructor(private sanitizer: DomSanitizer, public user: UserModel) { }

  ngOnInit() {

  }
}
