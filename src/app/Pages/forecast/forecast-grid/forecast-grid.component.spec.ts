import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastGridComponent } from './forecast-grid.component';

describe('ForecastGridComponent', () => {
  let component: ForecastGridComponent;
  let fixture: ComponentFixture<ForecastGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
