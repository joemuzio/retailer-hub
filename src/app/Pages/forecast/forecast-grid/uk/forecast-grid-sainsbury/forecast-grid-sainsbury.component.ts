import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {environment} from '../../../../../../environments/environment';
import {WebserviceController} from '../../../../../Services/WebserviceController';

@Component({
  selector: 'app-forecast-grid-sainsbury',
  templateUrl: './forecast-grid-sainsbury.component.html',
  styleUrls: ['./forecast-grid-sainsbury.component.css']
})
export class ForecastGridSainsburyComponent implements OnInit {
  @Input() dataArray = [];
  constructor(private sanitizer: DomSanitizer, private webservice: WebserviceController) { }

  ngOnInit() {

  }
  loadImageURL(event){
    if(!event.data.imageURL) {
      event.data.imageURL = this.sanitizer.bypassSecurityTrustUrl(environment.couponImageURL(event.data.promotionNumber, event.data.parentCorpKey));
    }
    // if(!event.data.imageURL){
    //   if (event.data.promotionNumber.match(/[a-z]/i)) {
    //     //Is 4x
    //     this.webservice.getVaultStaticImageURL(event.data.parentCorpKey, event.data.promotionNumber).subscribe(
    //       response => {
    //         for(var obj in response){
    //           if(response[obj]['static_image']){
    //             event.data.imageURL = this.sanitizer.bypassSecurityTrustResourceUrl(response[obj]['static_image']);
    //           }
    //         }
    //       },
    //       error => {
    //         event.data.imageURL = this.sanitizer.bypassSecurityTrustResourceUrl(environment.mcluCouponImageURL(event.data.parentCorpKey, "UK", 1));
    //       }
    //     )
    //   }
    //   else{
    //     //Is 2x
    //     event.data.imageURL = this.sanitizer.bypassSecurityTrustUrl(environment.mcluCouponImageURL(event.data.parentCorpKey, "UK", event.data.promotionNumber));
    //   }
    // }
  }
}
