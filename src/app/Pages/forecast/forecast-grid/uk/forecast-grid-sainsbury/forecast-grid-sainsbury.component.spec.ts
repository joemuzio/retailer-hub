import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastGridSainsburyComponent } from './forecast-grid-sainsbury.component';

describe('ForecastGridSainsburyComponent', () => {
  let component: ForecastGridSainsburyComponent;
  let fixture: ComponentFixture<ForecastGridSainsburyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastGridSainsburyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastGridSainsburyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
