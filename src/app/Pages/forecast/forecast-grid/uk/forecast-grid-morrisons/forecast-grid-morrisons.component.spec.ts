import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastGridMorrisonsComponent } from './forecast-grid-morrisons.component';

describe('ForecastGridMorrisonsComponent', () => {
  let component: ForecastGridMorrisonsComponent;
  let fixture: ComponentFixture<ForecastGridMorrisonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastGridMorrisonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastGridMorrisonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
