import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {environment} from '../../../../../../environments/environment';
import {WebserviceController} from '../../../../../Services/WebserviceController';

@Component({
  selector: 'app-forecast-grid-morrisons',
  templateUrl: './forecast-grid-morrisons.component.html',
  styleUrls: ['./forecast-grid-morrisons.component.css']
})
export class ForecastGridMorrisonsComponent implements OnInit {
  @Input() dataArray = [];

  constructor(private sanitizer: DomSanitizer, private webservice: WebserviceController) {
  }

  ngOnInit() {

  }

  loadImageURL(event){
    if(!event.data.imageURL) {
      event.data.imageURL = this.sanitizer.bypassSecurityTrustUrl(environment.couponImageURL(event.data.promotionNumber, event.data.parentCorpKey));
    }
  }
}
