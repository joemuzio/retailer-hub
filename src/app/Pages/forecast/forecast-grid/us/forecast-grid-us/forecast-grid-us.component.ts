import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {environment} from '../../../../../../environments/environment';
import {WebserviceController} from '../../../../../Services/WebserviceController';
import * as FileSaver from "file-saver";
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-forecast-grid-us',
  templateUrl: './forecast-grid-us.component.html',
  styleUrls: ['./forecast-grid-us.component.css']
})
export class ForecastGridUsComponent implements OnInit {
  busy: Subscription;
  @Input() dataArray = [];
  @Input() selectedRetailer:string = "";
  constructor(private sanitizer: DomSanitizer, private webservice: WebserviceController) { }

  ngOnInit() {
  }

  loadImageURL(event){
    if(!event.data.imageURL) {
          event.data.imageURL = this.sanitizer.bypassSecurityTrustUrl(environment.couponImageURL(event.data.promotionNumber, event.data.parentCorpKey));
      }
  }
  downloadUPCsButtonPressed(promo, triggering){
    if(triggering){
      this.busy = this.webservice.getTriggerUPCListForPromotion(promo).subscribe(
        response => {
          // console.log(response)
          var blob = new Blob([response], { type: 'text/csv' });
          FileSaver.saveAs(blob, "Triggering_" + promo + '.csv');
        },
        error => {
          alert(JSON.stringify("An error occured generating UPC List data - NO ROWS FOUND"));
        }
      )
    }
    else{
      this.busy = this.webservice.getValidatingUPCListForPromotion(promo).subscribe(
        response => {
          // console.log(response)
          var blob = new Blob([response], { type: 'text/csv' });
          FileSaver.saveAs(blob, "Validating_" + promo + '.csv');
        },
        error => {
          alert(JSON.stringify("An error occured generating UPC List data - NO ROWS FOUND"));
        }
      )
    }

    //alert(promo);
  }
  saveExportCSV(name){

  }
}
