import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastGridUsComponent } from './forecast-grid-us.component';

describe('ForecastGridUsComponent', () => {
  let component: ForecastGridUsComponent;
  let fixture: ComponentFixture<ForecastGridUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastGridUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastGridUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
