import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../Models/UserModel';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {
  constructor(public user: UserModel) {

  }
  ngOnInit(){

  }

}
