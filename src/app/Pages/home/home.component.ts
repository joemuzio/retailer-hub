import { Component, OnInit } from '@angular/core';
import {WebserviceController} from '../../Services/WebserviceController';
import {UserModel} from '../../Models/UserModel';
import {AnalyticsService} from '../../Services/AnalyticsService';
import {MSTRController} from '../../Services/MSTRController';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private webservice: WebserviceController, private user: UserModel,
              private analyticsService: AnalyticsService, private mstrController: MSTRController) { }
  ngOnInit() {
    this.webservice.updateUserProfile().subscribe(
      response => {
        var retailer = this.user.selectedRetailer;
        if(this.user.profile.isExternal != null && (this.user.profile.isExternal == "N" || this.user.profile.isExternal == "n")){
          retailer = "Internal_" + this.user.selectedRetailer;
        }
        this.analyticsService.sendRetailerName(retailer);
      },
      error => {
        console.log(JSON.stringify(error));
      });
  }
}
