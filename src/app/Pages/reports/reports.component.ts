import {Component, Input, OnInit} from '@angular/core';
import {UserModel} from '../../Models/UserModel';
import {ReportModel} from '../../Models/ReportModel';
import {SelectItem} from 'primeng/api';
import {Router} from '@angular/router';
import {WebserviceController} from '../../Services/WebserviceController';
import {MSTRController} from '../../Services/MSTRController';
import {Historylist} from '../../Models/HistoryListModel';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  allReports = [];
  filteredReports = [];
  reportCategories = [];
  selectedCategory;
  sortOptions: SelectItem[];
  sortKey: string;
  sortField: string;
  sortOrder: number;
  showHeader = true;
  constructor(private user: UserModel, private router: Router, private webservice: WebserviceController,
              private mstrController: MSTRController) {
  }

  ngOnInit() {
    this.allReports.push(this.createForecastReport());
    this.webservice.updateHistoryList().subscribe(
      response => {
        let historyList:Historylist = this.mstrController.historylist;
        console.log("History List = " + JSON.stringify(historyList));
        let historyListArray = this.getReportsFromHistoryList(historyList);
        this.allReports.push.apply(this.allReports, historyListArray);
        console.log(JSON.stringify(this.allReports));
        this.filteredReports = this.allReports;
        this.updateData();
      },
      error => {
        this.filteredReports = this.allReports;
        this.updateData();
      }
    );
    this.sortOptions = [
      {label: 'Newest First', value: '!reportDate'},
      {label: 'Oldest First', value: 'reportDate'},
      {label: 'Title', value: 'reportTitle'}
    ];
  }
  updateData(){
    var tempArray = this.allReports;
    this.reportCategories = this.getCategories(tempArray);
    this.filteredReports = this.filterCategories(tempArray);
  }
  getCategories(dataArray:Array<ReportModel>){
      var categoryArray = [];
      for(var i = 0; i < dataArray.length; i++){
        let report = dataArray[i];
        var contains = false;
          for(var j = 0; j < categoryArray.length; j++){
            let reportToCheck = categoryArray[j];
            if(report.reportCategory == reportToCheck.value){
              contains = true;
              break;
            }
          }
          if(contains == false){
            categoryArray.push({label: report.reportCategory, value: report.reportCategory});
          }
        }
      return categoryArray;
  }
  filterCategories(dataArray: Array<ReportModel>){
    if(this.selectedCategory && this.selectedCategory != ""){
      var reportArray = [];
      for(var i = 0; i < dataArray.length; i++){
        let report = dataArray[i];
        if(this.selectedCategory == report.reportCategory){
          reportArray.push(report);
        }
      }
      return reportArray;
    }
    else{ return dataArray; }
  }
  getReportsFromHistoryList(historyList){
    var tempArray = [];
    for(var i = 0; i < historyList.data.length; i++) {
      let reportParent = this.mstrController.historylist.data[i];
      for (var j = 0; j < reportParent.data.length; j++) {
        let reportChild = reportParent.data[j];
        var reportModel = new ReportModel();
        reportModel.reportCategory = reportParent.name;
        reportModel.reportIsMSTR = true;
        reportModel.reportTitle = reportChild.name;
        reportModel.reportDate = new Date(reportChild.creationTime);
        reportModel.reportID = reportChild.messageId;
        reportModel.reportURL = encodeURI(reportParent.name);
        reportModel.reportIconURL = "../../assets/images/receipt.png";
        reportModel.reportImage = reportParent.image;
        reportModel.items = this.createItems(reportModel.reportID);
        tempArray.push(reportModel);
        console.log("temp array length = " + tempArray.length);
      }
    }
    return tempArray;
  }
  selectReport(event: Event, report: ReportModel) {
    if(report.reportIsMSTR){
      this.router.navigate(["home/report/" + report.reportID]);
    }
    else{
      this.router.navigate(["home/" + report.reportURL])
    }
  }

  onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }
  getThumbURLForReport(report){
    if(report.reportIsMSTR){
      return environment.mstrThumbImageURL(report.reportImage);
    }
    else{
      return report.reportImage;
    }
  }
  createItems(reportID){
    return [{label: "Delete", command: (event) => {
        alert("deleting " + reportID + " for user " + this.user.profile.userName);
      }},
      {label: "Unsubscribe", command: (event) => {
          alert("unsubscribing " + reportID + " for user " + this.user.profile.userName);
        }}
    ];
  }
  createForecastReport(){
    var report = new ReportModel();
    report.reportCategory = "ACTIVITY CALENDAR";
    report.reportTitle = "Activity Calendar";
    report.reportDate = new Date();
    report.reportDate.setDate(report.reportDate.getDate() - 1);
    report.reportURL = "forecast";
    report.reportIsMSTR = false;
    report.reportImage = "../../assets/images/forecast-calendar.png";
    return report;
  }
  createActualsReport(reportID){
    var report = new ReportModel();
    report.reportCategory = "Distribution";
    report.reportTitle = "Actuals Dashboard";
    report.reportDate = new Date();
    report.reportDate.setDate(report.reportDate.getDate() - 1);
    report.reportIsMSTR = true;
    report.reportURL = "actuals";
    report.reportID = reportID;
    report.reportImage = "../../assets/images/receipt.png";
    return report;

  }
  headerShowHideButtonPressed(){
    this.showHeader = !this.showHeader;
  }
}
