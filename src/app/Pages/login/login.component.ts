import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {WebserviceController} from '../../Services/WebserviceController';
import {Router} from '@angular/router';
import {Dialog} from 'primeng/primeng';
import {UserModel} from '../../Models/UserModel';
import {ErrorComponent} from '../../Shared/error/error.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  busy: Subscription;
  @ViewChild(ErrorComponent) errorComponent: ErrorComponent;
  constructor(private webservice:WebserviceController, private user: UserModel,
              private router:Router) { }
  display: boolean = false;
  ngOnInit() {

  }
  login(username:string, password:string){
    username = username.trim().toLocaleLowerCase();
    this.busy = this.webservice.login(username, password).subscribe(
      response => {
        if(response['access_token']){
          this.user.authToken = response['access_token'];
          // this.user.userName = username;
          this.router.navigate(['home']);
        }
        else{
          console.log("Token missing from response")
        }
      },
      error => {
        this.errorComponent.showErrorModalForError(error);
      }
    )
  }
}
