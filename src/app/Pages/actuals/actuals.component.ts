import { Component, OnInit } from '@angular/core';
import {WebserviceController} from '../../Services/WebserviceController';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {environment} from '../../../environments/environment';
import {UserModel} from '../../Models/UserModel';
import {Router} from '@angular/router';

@Component({
  selector: 'app-actuals',
  templateUrl: './actuals.component.html',
  styleUrls: ['./actuals.component.css']
})
export class ActualsComponent implements OnInit {
  url:SafeResourceUrl;

  constructor(private webserviceController: WebserviceController, private user: UserModel,
              private sanitizer: DomSanitizer, private router: Router) { }

  ngOnInit() {
    this.getNARDistributionURLAndRToken();
  }

  getNARDistributionURLAndRToken(){
    this.webserviceController.getNARDistributionURLAndToken().subscribe(
      response => {
        let documentID = response[0].retailer.documentId;
        let mstrToken = response[1].data.token;
        let userID = this.user.profile.userName;
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(environment.actualsDashboardMSTRURL(documentID, userID, mstrToken));
      },
      error => {
        alert(JSON.stringify(error));
      }
    )
  }


}
