export const environment = {
  production: false,
  baseServicesURL: "http://stpdmuswslnxv04:28080",
  baseMSTRURL: "https://uatinsights.catalina.com",
  gaID: "UA-98580837-2",
  playlistFolderIds: ['EB03C18448D96FFA70F58D94E8E47F6B'],
  mstrServer: "orlubiapplv01",
  loginURL:function () {
    return this.baseServicesURL + "/RetailerPortal/oauth/token";
  },
  narForecastURL: function () {
    // if(retailer == "Morrisons" || retailer == "Sainsbury"){
    //   return this.baseServicesURL + "/RetailerPortal/reports/getNarForecastDataEU/"
    // }
    // return this.baseServicesURL + "/RetailerPortal/reports/getNarForecastDataCSV/";
    return this.baseServicesURL + "/RetailerPortal/reports/getCedw4DataForRetailer";
  },
  userDataURL: function () {
    return this.baseServicesURL + "/RetailerPortal/user/getCurrentUserDetails";
  },
  mstrTokenURL: function () {
    return this.baseServicesURL + "/RetailerPortal//secure/getMSTRTokenForUser/";
  },
  mstrSessionURL: function () {
    return this.baseServicesURL + "/RetailerPortal//secure/getMSTRSessionForUser/";
  },
  couponImageURL(promotionNumber, parentCorpKey){
    return this.baseServicesURL + "/RetailerPortal/images/getCouponImage?key=" + promotionNumber + "&parentcorpkey=" + parentCorpKey;
  },
  mstrThumbImageURL: function (image) {
    return this.baseMSTRURL + "/CI/plugins/Playlist/images/playlist/" + image;
  },
  switchRetailerURL: function (retailerID) {
    return this.baseServicesURL + "/RetailerPortal/retailer/setUserToRetailer?retailerId=" + retailerID;
  },
  playlistDataURL: function () {
    return this.baseServicesURL + "/RetailerPortal/mstr/playlist";
  },
  historyListURL: function () {
    return this.baseServicesURL + "/RetailerPortal/mstr/historylist";
  },
  actualsDashboardMSTRURL: function (documentID, userID, mstrToken) {
    return this.baseMSTRURL + "/CI/servlet/mstrWeb" +
    "?evt=3140&src=mstrWeb.3140&documentId=" + documentID + "&Server=ORLUBIAPPLV02" +
    "&Project=Catalina%20Insights%20-%20UAT&Port=0&share=1&hiddensections=header,path,dockTop,dockLeft,footer" +
    "vismode=0&currentViewMedia=8&mstrwid=1&loginReq=true&Uid=" + userID + "&cmcID=" + mstrToken
  },
  validatingUPCURL: function (promotionNumber) {
    // return this.baseServicesURL + "RetailerPortal/download/getUPCList?key=P0001"
    return this.baseServicesURL + "/RetailerPortal/download/getUPCList?key=" + promotionNumber;
  },
  triggeringUPCURL: function (promotionNumber) {
    return this.baseServicesURL + "/RetailerPortal/download/getTriggerUPCList?key=" + promotionNumber;
  }

};
